# Overview

## Index

[1. Usage](#usage)

[3. Testing](#testing)

[2. Examples](#examples)

[4. Docker](#docker)

---

## Usage (Server)

Using the application is fairly straightforward, follow the instructions below to get the server up and running


* Create a `.env` file on the root of the project
* Add a `MONGODB_URL` entry inside, add in the MongoDB URL
* (Optional) Add in a `PORT` variable if you don't want to run it in `8080`
* Then lastly add in a `DB_NAME` entry in the `.env` file with db name you wish to use
* Simply run `npm start` afterwards

## Usage (API)

While most of the detailed information is right below, it's pretty simple. By following the requirements below, first get a JWT token at `/auth`, then you can use that token in your other requests at `/records` endpoint. Without it, `/records` will reject your HTTP request.

### Endpoints

The app uses JWT for authentication, therefore using the `/auth` endpoint beforehand is required in order to use the main endpoint

- `/auth` -> Responsible for JWT authentication
- `/records` -> Main endpoint where the aggregation happens

### ```/records```
Required parameters are:
- `startDate` {String} in 'YYYY-MM-DD' format
- `endDate` {String} in 'YYYY-MM-DD' format
- `minCount` {Number}, can't be negative and can't be more than `maxCount`
- `maxCount` {Number}, can't be negative, has to be more than `minCount`

### ```/auth```
Required parameters are:
- `accessString` {String} could be anything

This endpoint returns a token that should be preserved in order to use `/records` endpoint.
The JWT token should be in the headers as `Bearer` authentication method. 

## Testing

I'll be honest here, due to my 'limited' testing knowledge and experience, I tried my best to cover many of the unit tests but it's not as good as I'd want it to be. This made me realize the gap in my knowledge and I'm looking forward to improve it already.

For testing, by running `npm test` you can run through the unit tests of `/records` API Endpoint's validations and some of the JWT functionalities
