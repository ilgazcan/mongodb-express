require('dotenv').config();

const jwt = require('jsonwebtoken');

/**
 * 
 * @summary Verifies user authentication via JWT
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 * @description verifies that before the api call hits /records endpoint
 * that request must has a valid JWT token  
 */
const verify = (req, res, next) => {
  const headers = req.headers;
  const token = headers.authorization && headers.authorization.split(' ')[1];

  if (!token) {
    next({code: 403, msg: 'Auth token not found'});
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, _) => {
    if (err) res.send({code: 403, msg: 'Could not verify token'});
    next();
  });
}

/**
 * 
 * @summary Signs the {accessString} with JWT_SECRET from environment variables
 * @param {String} accessString 
 * @returns {String} JWT Token
 */
const createToken = accessString => jwt.sign({accessString}, process.env.JWT_SECRET, { expiresIn: "21d" });

module.exports = {
  verify,
  createToken
}
