const {
  PropertyValidationError,
  MissingPropertyError,
  InvalidCountError,
  InvalidDateFormatError,
  InvalidDateCombinationError
} = require('../errors/');

const moment = require('moment');


/**
 * @summary Checks the request property for possible errors
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 * @throws {MissingPropertyError}
 * @throws {InvalidCountError}
 * @throws {InvalidDateError}
 * @throws {PropertyValidationError}
 * @description Since the aggregation requires all four properties
 * to be present and valid, it's best to check for all possibilities.
 * This function (middleware, kinda) looks for logical errors such as
 * the user entering lower maxCount than minCount, wrong date formatting 
 * (non YYYY-MM-DD) and missing properties
 * 
 * @todo Small improvement note, `moment` is a huge package and swapping it
 * or writing it from scratch should let us have better package sizes
 * and help us get rid of unnecessary 3rd party dependencies
 */
module.exports = function recordsValidator(req, _, next) {
  const requiredProps = ['startDate', 'endDate', 'minCount', 'maxCount'];

  requiredProps.forEach(prop => {
    if (req.body && !req.body.hasOwnProperty(prop)) {
      throw new MissingPropertyError(prop);
    };
  })

  if (isNaN(req.body.minCount) || req.body.minCount < 0) {
    throw new InvalidCountError("minCount");
  }

  if (isNaN(req.body.maxCount) || req.body.maxCount < 0) {
    throw new InvalidCountError("maxCount");
  }

  if (req.body.minCount > req.body.maxCount) {
    throw new PropertyValidationError(`Minimum count can't be bigger than maximum`);
  }

  if (!moment(req.body.startDate, 'YYYY-MM-DD', true).isValid()) {
    throw new InvalidDateFormatError('startDate');
  }

  if (!moment(req.body.endDate, 'YYYY-MM-DD', true).isValid()) {
    throw new InvalidDateFormatError('endDate');
  }

  if (moment(req.body.startDate).isAfter(req.body.endDate)) {
    throw new InvalidDateCombinationError(null);
  }

  next();
};
