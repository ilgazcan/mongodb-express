const bodyParser = require('body-parser');
const recordsValidator = require('./recordsValidator');
const {verify: authValidator} = require('./auth');

/**
 * @summary Catch-all middleware for error handling
 * @param {import('express').Errback} err 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 * @returns {import('express').Response} Express' built-in response
 * 
 * @description Catches errors on /records endpoint, sends back a descriptive
 * error message back to user with status code, explanation and
 * for consistency, an empty `records` property

 * @see {@link https://expressjs.com/en/guide/error-handling.html} for more information
 * and why all four parameters are required
  */
const errorLogger = (err, req, res, next) => {
  return res
    .json({
      code: err.code || 500,
      msg: err.msg || 'Something went wrong.',
      records: []
    });
};

/**
 * @summary Mounts pre-defined middlewares to express endpoints
 * @param {import('express').Application} app ExpressJS App instance
 * @returns {void}
 * 
 * @description It mounts middleware(s) by using `app.use` to specific
 * endpoints. Also utilizes the imports on this file to keep the entrypoint
 * of this module cleaner. 
 */
const mountMiddlewares = app => {
  app.use(bodyParser.json());

  app.use('/records', [authValidator, recordsValidator]);
  app.use(errorLogger)
};

module.exports = mountMiddlewares
