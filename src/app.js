const express = require('express');

const mountMiddlewares = require('./middlewares/index');
const {createToken} = require('./middlewares/auth');
const {findDocumentsWithinRange} = require('./mongo');

const app = express();

mountMiddlewares(app);

/**
 * @api {Post} /records Gets records in predefined MongoDB collection
 *
 * @apiParam {String} startDate Date filter for aggregation
 * @apiParam {String} endDate Date filter for aggregation
 * @apiParam {Number} minCount minimum count that the inner document must contain 
 * @apiParam {Number} maxCount maximum count that the inner document must contain 
 *
 * @apiSuccess {Number} code Custom result code of the request
 * @apiSuccess {String} msg  Description about the result
 * @apiSuccess {Object[]} records The fetched records
 */
app.post('/records', (req, res, next) => {
  const response = {
    code: 0,
    msg: 'success',
    records: []
  };

  return findDocumentsWithinRange(req.body)
    .then(data => {
      response.records = data;
      res.json(response);
    })
    .catch(err => {
      console.error({err});

      res.json({
        ...response,
        code: 500,
        msg: 'Error occured while getting data',
      })
    });
});

/**
 * @api {Post} /auth Provides JWT token to use in the app
 *
 * @apiParam {String} accessString String to be used in JWT token signing
 *
 * @apiSuccess {String} token JWT token that users should use to authenticate
 */
app.post('/auth', (req, res, next) => {
  const {accessString} = req.body;

  if (!accessString) {
    res.json({
      code: 403,
      msg: 'You did not provide the access string for JWT',
    })
  }

  return res.json(createToken(accessString));
});

module.exports = app;
