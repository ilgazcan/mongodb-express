class PropertyValidationError extends Error {
  constructor(property) {
    super(property);

    this.name = this.constructor.name;
    this.code = 422;
    this.msg = property;

    Error.captureStackTrace(this, this.constructor);
  }
}

class MissingPropertyError extends PropertyValidationError {
  constructor(property) {
    super(`Property not found: ${property}`);

    this.msg = `Property not found: ${property}`;
  }
}

class InvalidDateFormatError extends PropertyValidationError {
  constructor(property) {
    super(`Wrong date format: ${property}`);

    this.msg = `Wrong date format: ${property}`;
  }
}

class InvalidDateCombinationError extends PropertyValidationError {
  constructor() {
    super(`endDate can't be earlier than startDate`);

    this.msg = `endDate can't be earlier than startDate`;
  }
}

class InvalidCountError extends PropertyValidationError {
  constructor(property) {
    super(`${property} is not a valid count entry`);

    this.msg = `${property} is not a valid count entry`;
  }
}

module.exports = {
  PropertyValidationError,
  MissingPropertyError,
  InvalidDateFormatError,
  InvalidDateCombinationError,
  InvalidCountError
}
