class DBConnectionError extends Error {
  constructor(message) {
    super('Connection to MongoDB Failed');

    this.statusCode = 500; // or 503
    this.message = message;
  }
};

module.exports = { DBConnectionError };
