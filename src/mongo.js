require('dotenv').config();

const { MongoClient } = require('mongodb');
const { DBConnectionError } = require('./errors/');

const MONGODB_URL = process.env.MONGODB_URL;
const DB_NAME = process.env.DB_NAME;
const COLLECTION_NAME = 'records';

let db;

/**
 * @returns {Promise} Promise that resolves when db instance is copied to `db` variable
 * @throws {DBConnectionError}
 * @description connects to the Mongo client and sets property `db`
 * which is reused by other functions in the file
 */
const connectMongo = () => new Promise((resolve) => {

  let client = new MongoClient(MONGODB_URL, {useUnifiedTopology: true});

  return client.connect()
    .then(() => {
      db = client.db(DB_NAME)
      resolve();
    })
    .catch(err => {
      throw new DBConnectionError(err.message);
    })
});

/**
 * @typedef {Object} AggregationProperties
 * @property {Number} AggregationProperties.minCount minimum count that the inner document must contain
 * @property {Number} AggregationProperties.maxCount maximum count that the inner document must contain
 * @property {String} AggregationProperties.startDate filter that's used to aggregate documents
 * @property {String} AggregationProperties.endDate filter that's used to aggregate documents
 *
 * @summary Queries and aggregates for documents that meet restrictions in parameter
 * @param {AggregationProperties} options Object that contains properties to aggregate the collection
 * @throws {Error} MongoDB internals throw an error when it fails (see below for more info)
 *
 * @description Where main logic resides. Here the documents within the `COLLECTION_NAME` collection
 * get queried and aggregated by the object in parameter. There's no prop checking here since a
 * middleware checks the `req` object right before the route associated with this function gets hit.
 *
 * @see {@link https://docs.mongodb.com/manual/reference/method/db.collection.aggregate/#behavior}
 * There's a specified error but no error kind
 *
 * @see Currently the `toArray` method is very costly on its own. When fetching a huge amount of data,
 * the performance will definitely suffer. Using a stream API will be much better here
 */

const findDocumentsWithinRange = async options => {

  if (!db) {
    throw new DBConnectionError("DB is not connected");
  }

  const {minCount, maxCount, startDate, endDate} = options;

  const aggregation = [
    {
      "$project": {
        "key": "$key",
        "createdAt": "$createdAt",
        "totalCount": {$sum: "$counts"},
        "_id": 0
      }
    },
    {
      "$match": {
        "totalCount": {
          "$gte": minCount,
          "$lte": maxCount
        },
        "createdAt": {
          "$gte": new Date(startDate),
          "$lte": new Date(endDate)
        }
      }
    }
  ];

  return db.collection(COLLECTION_NAME).aggregate(aggregation).toArray();
};

module.exports = {
  connectMongo,
  findDocumentsWithinRange
};
