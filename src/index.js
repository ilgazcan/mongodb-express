require('dotenv').config();

const app = require('./app');
const { connectMongo } = require('./mongo');

const PORT = process.env.PORT;

connectMongo()
  .then(() => {
    app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
  })
  .catch(err => {
    console.error(err);
  });
