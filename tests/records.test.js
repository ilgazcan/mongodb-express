const app = require('../src/app');
const errors = require('../src/errors');
const recordsValidator = require('../src/middlewares/recordsValidator');

const {
  InvalidCountError,
  InvalidDateFormatError,
  InvalidDateCombinationError,
  PropertyValidationError,
  MissingPropertyError
} = errors;

const validRequestBody = {
  startDate: new Date('2020-11-11'),
  endDate: new Date('2020-11-13'),
  minCount: 1,
  maxCount: 500
}

describe('Tests for /records', () => {
  let mockRequest = {};

  beforeEach(() => {
    mockRequest = { body: { ...validRequestBody } }
  });

  describe("Property validation", () => {
    test("Should throw PropertyValidationError when request body is missing property -> startDate", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          startDate: undefined
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(PropertyValidationError)
    })

    test("Should throw PropertyValidationError when request body is missing property -> endDate", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          endDate: undefined
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(PropertyValidationError)
    })

    test("Should throw PropertyValidationError when request body is missing property -> minCount", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          minCount: undefined
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(PropertyValidationError)
    })

    test("Should throw PropertyValidationError when request body is missing property -> maxCount", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          maxCount: undefined
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(PropertyValidationError)
    })

    test("Should throw PropertyValidationError when minCount is higher than maxCount", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          maxCount: 5,
          minCount: 20
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(PropertyValidationError)
    });

    test("Should throw InvalidCountError when maxCount is negative", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          maxCount: -20
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidCountError)
    });

    test("Should throw InvalidCountError when when minCount is negative", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          minCount: -20
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidCountError)
    });

    test("Should throw InvalidCountError when maxCount is not a number", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          maxCount: "test"
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidCountError)
    });

    test("Should throw InvalidCountError when minCount is not a number", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          minCount: "test"
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidCountError)
    });

    test("Should throw InvalidDateFormatError when startDate is not in correct format", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          startDate: '13-11-2020'
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidDateFormatError)
    });

    test("Should throw InvalidDateFormatError when endDate is not in correct format", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          endDate: '13-11-2020'
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidDateFormatError)
    });

    test("Should throw InvalidDateCombinationError when endDate is not in correct format", () => {
      mockRequest = {
        body: {
          ...validRequestBody,
          endDate: '2020-11-11',
          startDate: '2020-12-12'
        }
      }

      expect(() => recordsValidator(mockRequest, null, () => { }))
        .toThrow(InvalidDateCombinationError)
    });
  });

})


