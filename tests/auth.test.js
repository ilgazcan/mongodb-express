const auth = require('../src/middlewares/auth');

describe('Tests for authorization handlers & utils', () => {
  mockNext = jest.fn();

  test("verify fn calls res.send middleware when there's no authentication token", () => {
    const mockRes = {send: jest.fn()};

    auth.verify({headers: {}}, mockRes, mockNext);

    expect(mockRes.send).toHaveBeenCalled();
  });

  test("verify fn sends code 403 in body when there's no authentication", () => {
    const mockRes = {send: jest.fn()};
    const originalResponse = {code: 403, msg: "Could not verify token"}

    auth.verify({headers: {}}, mockRes, mockNext);

    expect(mockRes.send).toHaveBeenCalledWith(originalResponse);

  })
})
